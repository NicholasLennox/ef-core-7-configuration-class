﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EFCore6Class.Migrations
{
    /// <inheritdoc />
    public partial class changedalltonull : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Project_Student_StudentId",
                table: "Project");

            migrationBuilder.DropForeignKey(
                name: "FK_Student_Professor_ProfessorId",
                table: "Student");

            migrationBuilder.DropIndex(
                name: "IX_Project_StudentId",
                table: "Project");

            migrationBuilder.AlterColumn<int>(
                name: "ProfessorId",
                table: "Student",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "StudentId",
                table: "Project",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "IX_Project_StudentId",
                table: "Project",
                column: "StudentId",
                unique: true,
                filter: "[StudentId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Project_Student_StudentId",
                table: "Project",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Student_Professor_ProfessorId",
                table: "Student",
                column: "ProfessorId",
                principalTable: "Professor",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Project_Student_StudentId",
                table: "Project");

            migrationBuilder.DropForeignKey(
                name: "FK_Student_Professor_ProfessorId",
                table: "Student");

            migrationBuilder.DropIndex(
                name: "IX_Project_StudentId",
                table: "Project");

            migrationBuilder.AlterColumn<int>(
                name: "ProfessorId",
                table: "Student",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "StudentId",
                table: "Project",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Project_StudentId",
                table: "Project",
                column: "StudentId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Project_Student_StudentId",
                table: "Project",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Student_Professor_ProfessorId",
                table: "Student",
                column: "ProfessorId",
                principalTable: "Professor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

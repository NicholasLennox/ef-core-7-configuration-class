﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EFCore6Class.Migrations
{
    /// <inheritdoc />
    public partial class seedingProf : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Professor",
                columns: new[] { "Id", "Field", "Name" },
                values: new object[] { 1, "Fhesheshist", "John" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Professor",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}

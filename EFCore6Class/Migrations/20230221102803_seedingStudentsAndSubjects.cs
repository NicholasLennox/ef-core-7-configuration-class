﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EFCore6Class.Migrations
{
    /// <inheritdoc />
    public partial class seedingStudentsAndSubjects : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Student",
                columns: new[] { "Id", "Name", "ProfessorId" },
                values: new object[] { 1, "Bobby", null });

            migrationBuilder.InsertData(
                table: "Subject",
                columns: new[] { "Id", "Code", "Name", "ProfessorId" },
                values: new object[] { 1, "ABCD123", "Advanced advancing for novices", null });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Student",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Subject",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}

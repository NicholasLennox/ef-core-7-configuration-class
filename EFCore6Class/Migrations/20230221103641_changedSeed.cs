﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EFCore6Class.Migrations
{
    /// <inheritdoc />
    public partial class changedSeed : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Professor",
                keyColumn: "Id",
                keyValue: 1,
                column: "Field",
                value: "Phesheshist");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Professor",
                keyColumn: "Id",
                keyValue: 1,
                column: "Field",
                value: "Fhesheshist");
        }
    }
}

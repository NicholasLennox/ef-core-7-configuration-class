﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EFCore6Class.Migrations
{
    /// <inheritdoc />
    public partial class seededSubjects : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_StudentSubject",
                table: "StudentSubject");

            migrationBuilder.DropIndex(
                name: "IX_StudentSubject_SubjectsId",
                table: "StudentSubject");

            migrationBuilder.DeleteData(
                table: "Subject",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.AddPrimaryKey(
                name: "PK_StudentSubject",
                table: "StudentSubject",
                columns: new[] { "SubjectsId", "StudentsId" });

            migrationBuilder.InsertData(
                table: "Subject",
                columns: new[] { "Id", "Code", "Name", "ProfessorId" },
                values: new object[] { 2, "ABCD123", "Advanced advancing for novices", null });

            migrationBuilder.InsertData(
                table: "StudentSubject",
                columns: new[] { "StudentsId", "SubjectsId" },
                values: new object[] { 1, 2 });

            migrationBuilder.CreateIndex(
                name: "IX_StudentSubject_StudentsId",
                table: "StudentSubject",
                column: "StudentsId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_StudentSubject",
                table: "StudentSubject");

            migrationBuilder.DropIndex(
                name: "IX_StudentSubject_StudentsId",
                table: "StudentSubject");

            migrationBuilder.DeleteData(
                table: "StudentSubject",
                keyColumns: new[] { "StudentsId", "SubjectsId" },
                keyValues: new object[] { 1, 2 });

            migrationBuilder.DeleteData(
                table: "Subject",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.AddPrimaryKey(
                name: "PK_StudentSubject",
                table: "StudentSubject",
                columns: new[] { "StudentsId", "SubjectsId" });

            migrationBuilder.InsertData(
                table: "Subject",
                columns: new[] { "Id", "Code", "Name", "ProfessorId" },
                values: new object[] { 1, "ABCD123", "Advanced advancing for novices", null });

            migrationBuilder.CreateIndex(
                name: "IX_StudentSubject_SubjectsId",
                table: "StudentSubject",
                column: "SubjectsId");
        }
    }
}

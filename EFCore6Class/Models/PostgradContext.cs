﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCore6Class.Models
{
    internal class PostgradContext : DbContext
    {
        public DbSet<Professor> Professors { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Subject> Subjects { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source = DESKTOP-HH0G8JM\\SQLEXPRESS; Initial Catalog = PostgradEF; Integrated Security = True; Trust Server Certificate = True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Professor>().HasData(
                new Professor() { Id = 1, Name = "John", Field = "Phesheshist"}
                );
            modelBuilder.Entity<Student>().HasData(
                new Student() { Id = 1, Name = "Bobby" }
                ); ;
            modelBuilder.Entity<Subject>().HasData(
                new Subject() { Id = 2, Code = "ABCD123", Name = "Advanced advancing for novices"}
                );
            modelBuilder.Entity<Student>()
                .HasMany(s => s.Subjects)
                .WithMany(sb => sb.Students)
                .UsingEntity<Dictionary<string, object>>(
                    "StudentSubject",
                    r => r.HasOne<Subject>().WithMany().HasForeignKey("SubjectsId"),
                    l => l.HasOne<Student>().WithMany().HasForeignKey("StudentsId"),
                    je =>
                    {
                        je.HasKey("SubjectsId", "StudentsId");
                        je.HasData(
                            new { SubjectsId = 2, StudentsId = 1 });
                    });
        }
    }
}

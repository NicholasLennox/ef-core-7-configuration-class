﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCore6Class.Models
{
    [Table("Professor")]
    internal class Professor
    {
        public int Id { get; set; }
        [MaxLength(100)]
        public string Name { get; set; } = null!;
        [MaxLength(50)]
        public string? Field { get; set; }

        // Navigation
        public ICollection<Student> Students { get; set; } = null!;
        public ICollection<Subject> Subjects { get; set; } = null!;
    }
}

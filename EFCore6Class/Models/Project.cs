﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCore6Class.Models
{
    [Table("Project")]
    internal class Project
    {
        public int Id { get; set; }
        public string Title { get; set; } = null!;

        // Relationships
        public int? StudentId { get; set; }
        public Student Student { get; set; } = null!;

    }
}

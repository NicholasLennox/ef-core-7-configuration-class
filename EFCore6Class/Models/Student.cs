﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCore6Class.Models
{
    [Table("Student")]
    internal class Student
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;

        // Navigation properties
        public Project Project { get; set; } = null!;

        public int? ProfessorId { get; set; }
        public Professor Professor { get; set; } = null!;
        public ICollection<Subject> Subjects { get; set; } = null!;
    }
}

﻿using Microsoft.Extensions.Logging.Abstractions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCore6Class.Models
{
    [Table("Subject")]
    internal class Subject
    {
        public int Id { get; set; }
        public string Code { get; set; } = null!;
        public string Name { get; set; } = null!;

        // Navigation
        public int? ProfessorId { get; set; }
        public Professor Professor { get; set; } = null!;
        public ICollection<Student> Students { get; set; } = null!;

    }
}
